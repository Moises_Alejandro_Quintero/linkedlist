
typedef struct node Node;

struct node{
    int data;
    Node *next;
};

void initialize_list(Node **root, int value);
int getLength_list(Node *root);
void append_list(Node **root, int value);
void print_list(Node *root);
void remove_fromlist(Node **root, int value);