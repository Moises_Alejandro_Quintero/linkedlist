#include "../src/linked_list.c"

typedef struct list_mq List;

struct list_mq{
    void (*init)(Node **root, int value);
    int (*getlength)(Node *root);
    void (*append)(Node **root, int value);
    void (*print)(Node *root);
    void (*remove)(Node **root, int value);
};

Node *head;
int value;