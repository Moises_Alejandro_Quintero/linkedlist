#include "src/List.c"
//#include "src/linked_list.c"
#include <stdio.h>

int main()
{
    /*
    Node *head;
    head = 0;
    append_list(&head, 100);
    append_list(&head, 101);
    append_list(&head, 102);
    //append_list(&head, 103);
    //append_list(&head, 104);
    //append_list(&head, 105);
    printf("Number of items in list: %d\n", getLength_list(head));
    
    print(head);
    
    printf("Removing value 101\n");
    remove_fromlist(&head, 102);
    printf("Number of items in list: %d\n", getLength_list(head));
    
    print(head);
    */

    /*
    Instance object List_class has been created in "List.c"
    List_class instance has function pointer members to 
    functions defined in Linked_List.c

    The purpose of creating an instance object is to use the same 
    root header to keep track of the same pointer.
    */
    List_class.append(&head, 100);
    List_class.append(&head, 101);
    List_class.append(&head, 102);
    List_class.print(head);
    List_class.remove(&head, 102);
    List_class.print(head);
    return 0;
}