#include "../h/linked_list.h"
#include <stdlib.h>
#include <stdio.h>

void initialize_list(Node **root, int value){
    (*root) = (Node *)malloc(sizeof(Node));
    (*root)->data = value;
    (*root)->next = 0;
}

int getLength_list(Node *root){
    if(root == 0){
        return 0;
    }
    else{
        int count = 0;
        Node *current = root;
        while(current != 0){
            count++;
            current = current->next;
        }
        return count;
    }
}

void append_list(Node **root, int value){
    if(*root == 0)
    {
        initialize_list(root, value);
    }
    else{
        Node *temp;
        temp = (Node *)malloc(sizeof(Node));
        temp->data = value;
        temp->next = 0;

        Node *current = *root;
        while(current->next != 0)
        {
            current = current->next;
        }
        current->next = temp;
        current = current->next;
    }
}

void print_list(Node *root){
    Node *current = root;
    while(current != 0){
        printf("%d->",current->data);
        current = current->next;
    }
    printf("\n");
}

void remove_fromlist(Node **root, int value){
    if(root == 0){
        return;
    }
    else{
        if((*root)->next == 0){
            if((*root)->data == value)
                root = 0;
        }else{
            Node *current = *root;
            Node *previous = current;
            while(current != 0)
            {
                if(current->next != 0)
                {
                    if(current->data == value){
                        printf("Removal here, P: %d C: %d\n\n", previous->data, current->data);
                        previous->next = current->next;
                        previous = previous->next;
                        current = current->next;
                        return;
                    }
                }
                else{
                    if(current->data == value){
                        previous->next = 0;
                        current = 0;
                        return;
                    }
                }
                previous = current;
                current = current->next;
            }
        }
    }
}